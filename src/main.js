import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		menu: [
			{ link: "/", label: "Home" },
			{
				link: "/departments",
				label: "Departments",
				sub: [
					{ link: "//wildwoodhealth.com", label: "Lifestyle Center" },
					{ link: "//healthevangelism.com", label: "Medical Missionary Training" },
					{ link: "/natural-food-market/", label: "Natural Food Market" },
					{ link: "/natural-farm/", label: "Natural Farm" },
					{ link: "/clinic/", label: "Clinic" },
					{ link: "//observer.wildwoodhealth.com", label: "Medical Observer Program" },
				]
			},
			{ link: "/careers", label: "Careers" },
			{ link: "/news", label: "News" },
			{ link: "/about", label: "About" },
			{ link: "/contact", label: "Contact" },
			{ link: "/give", label: "Give", class: "donate" },
			{ link: "/login", label: "Login" },
		],
		originalProjects: [{
			title: "Worthy Student Fund",
			img: "worthy-student-fund",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/worthy-student-fund",
			desc: "Make a student's dream to become a missionary come true. Sponsor a student's education at the Center for Health Evangelism by donating toward the worthy student fund. This will help someone take a course in Health Evangelism, Ministry Management (business evangelism), or Lifestyle Coaching.",
			categories: ["Educational Funds"],
		},
		{
			title: "Bible Work",
			img: "bible-worker",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/bible-work-in-dade-county",
			desc: "Many people in our area do not have Bibles and have several physical needs. God has given us the opportunity to serve these communities by following His example as we make new friends going door to door. These new friends open their hearts to the precious truths of the Bible as they see our commitment to help them. This ministry is growing and we are praying for your support to purchase more literature so we can continue to meet the needs and expenses related to Bible work. Please pray about donating to this wonderful cause. Help us reach as many people as we can!",
			categories: ["Bible Work"],
		},
		{
			title: "Better Living Center",
			img: "better-living-center",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/better-living-center",
			goal: "350000",
			desc: "Our most important initiative is more outreach in our local communities. The Better Living Center will be the hub for events such as cooking classes, natural remedies seminar, “Dinner with the Doctor”, and many other educational and planned activities for our communities. With two-thirds of the cost already received we have about $350,000 to complete the BLC.",
			categories: ["Campus Infrastructure"],
		},
		{
			title: "Roads",
			img: "roads",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/roads",
			goal: 300000,
			desc: "Our campus is over 75 years old. Many of the roads on our campus need to be repaved. There are also many areas of our campus that are only accessed via gravel roads with plenty of holes. New roads would mean cutting down on costs for campus vehicle repair significantly, as well as having safer roads to travel in emergency situations.",
			categories: ["Campus Infrastructure"],
		},
		{
			title: "Student Cafe",
			img: "student-cafe",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/student-cafe",
			goal: 60000,
			desc: "A growth in student body calls for an expansion of the cafeteria. This allows for more efficient storage of fresh produce, and a reduced waiting time for students before they can be seated.",
			categories: ["Educational Funds"],
		},
		{
			title: "Spa Renovation",
			img: "spa-renovation",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/spa-renovation",
			goal: 30000,
			desc: "Renovating the spa area would greatly enhance the guest experience at Wildwood Lifestyle Center. Guests would be able to enjoy a true spa experience with a new sauna, massage tables, and jacuzzi. Not only does this improve their health, a renovated spa also means aiding in guests' regeneration process.",
			categories: ["Lifestyle Renovations"],
		},
		{
			title: "Campus Housing",
			img: "campus-housing",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/campus-housing",
			goal: 120000,
			desc: "We are incredibly thankful for a growth in our staff, but this also puts us face-to-face with some challenges in housing everyone. We plan to build three two bedroom houses that will accommodate our current needs.",
			categories: ["Campus Infrastructure"],
		},
		{
			title: "W.D. Frazee Historical Restoration",
			img: "wd-frazee-historical-restoration-fund",
			link: "https://wdfsermons.org/needs/",
			goal: 24000,
			desc: "Elder W.D. Frazee was one of the founders of Wildwood. He has left behind a legacy in evangelism and medical missionary work. Our W.D. Frazee Sermons team has been able to transcribe more than 1,600 of his messages so far, but a lot of those transcriptions need some final editing. We need $24,000 dollars to pay transcriptionists, so that our team can finish this work in the next 18 months.",
			categories: ["Educational Funds"],
		},
		{
			title: "Lifestyle Room Renovation",
			img: "lifestyle-room-renovation",
			link: "https://secure.givelively.org/donate/wildwood-sanitarium-incorporated/lifestyle-room-renovation",
			goal: 6000,
			desc: "With constant use renovation of the rooms at the lifestyle center is inevitable. We want our guests to not just be comfortable in their rooms, we want them to feel at home. Currently there are two rooms that we would like to update. We need about $6,000 per room.",
			categories: ["Lifestyle Renovations"],
		},
		]
	}
});

export default app;